### succeeded

```
mvn -Pdataflow-runner compile exec:java \
-Dexec.mainClass=org.apache.beam.examples.WordCount \
-Dexec.args="--project=pcb-poc-datalake1 \
--gcpTempLocation=gs://pcb-poc-datalake1-bucket-danielz/temp/ \
--output=gs://pcb-poc-datalake1-bucket-danielz/output \
--runner=DataflowRunner \
--region=northamerica-northeast1 \
--subnetwork=regions/northamerica-northeast1/subnetworks/poc-subnet-a \
--usePublicIps=false"


mvn -Pdataflow-runner compile exec:java \
-Dexec.mainClass=org.apache.beam.examples.WordCount \
-Dexec.args="--project=pcb-poc-datalake1 \
--gcpTempLocation=gs://pcb-poc-datalake1-bucket-danielz/temp/ \
--output=gs://pcb-poc-datalake1-bucket-danielz/output \
--runner=DataflowRunner \
--region=northamerica-northeast1 \
--subnetwork=regions/northamerica-northeast1/subnetworks/poc-vpc-subnet-1 \
--usePublicIps=false"
```

## make template

```
mvn -Pdataflow-runner compile exec:java \
 -Dexec.mainClass=org.apache.beam.examples.WordCount \
 -Dexec.args="--runner=DataflowRunner \
              --project=pcb-poc-datalake1 \
              --output=gs://pcb-poc-datalake1-bucket-danielz/output \
              --stagingLocation=gs://pcb-poc-datalake1-bucket-danielz/staging \
              --templateLocation=gs://pcb-poc-datalake1-bucket-danielz/templates/WORD_COUNT \
              --region=northamerica-northeast1 \
              --subnetwork=regions/northamerica-northeast1/subnetworks/poc-vpc-subnet-1 \
              --usePublicIps=false"

```